Voici le 3ème projet de l'année, un projet en groupe de 3. Le but est de réaliser un calendrier en utilisant Moment.JS. 

En terme d'organisation, nous sommes restés sur le principe des milestones et du board sur Gitlab. De cette manière, nous avons pu suivre un plan et une organisation. 

Avant tout travail technique de codage, nous avons pris quelques heures pour affiner notre plan et se familiariser avec l'outil Moment.JS.











## User Stories

1. En tant qu'utilisateur.ice, je veux pouvoir visualiser tous mes événements du mois en cours afin de pouvoir m'organiser à court terme

2. En tant qu'utilisateur.ice, je veux pouvoir créer de nouveaux événements afin de ne pas oublier de futurs rendez-vous

3. En tant qu'utilisateur.ice, je veux pouvoir modifier les événements existants afin de pouvoir annuler ou reporter certains d'entre eux

4. En tant qu'utilisateur.ice , je souhaite  avoir un rappel me sigalant les anniversaires de mes contacts.

   ## Use Cases , Maquette , Class 

   

![UseCaseDiagram1](./Images/UseCaseDiagram1.png)





![Untitled-Diagram-calendar](./Images/ClassDiagram1.png)