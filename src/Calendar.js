import { Evenement } from "./Evenement";
import moment from "moment";
import { log } from "util";

export class Calendar {

    constructor() {

        this.month = moment();
        this.event = [];

    }





    addMonth() {
        this.month.add(1, 'month');
        this.draw();
    }


    minusMonth() {
        this.month.add(-1, 'month');
        this.draw()
    }

    draw() {


        let header = document.querySelector('#date');
        header.innerHTML = this.month.format('MMMM YY');
        let days = document.querySelectorAll('.days');
        let daysArray = [];
        for (const iterator of days) {
            iterator.textContent = '';
            daysArray.push(iterator)
        }

        this.month.date(1);
        daysArray.splice(0, this.month.day());
        for (let index = 0; index < this.month.daysInMonth(); index++) {
            this.month.date(index + 1)
            //A chaque tour, assigner l'index + 1 comme jour du mois de ta date


            //Ensuite utiliser le .day() de la date, pour l'additionner à l'index entre les crochets uniquement
            daysArray[index].textContent = index + 1;
            index + this.month.day();
            console.log(index + this.month.day());

        }


    }
    displayCurrentDate() {

        document.getElementById("dateJour").innerHTML = moment().format('DD MMMM HH:mm');
    }
}
